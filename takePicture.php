<?php 
exec('"C:\Program Files (x86)\digiCamControl\CameraControlCmd.exe" capture');

$saveDirectory="E:\Images\Default\\";
// $saveDirectory="C:\users\igotcha\Pictures\Default\\";
$localDirectory="C:\wamp\www\Igotcha\photobooth2\imagesDirectory\\";

$files = scandir($saveDirectory, SCANDIR_SORT_DESCENDING);
$locfiles = scandir($localDirectory, SCANDIR_SORT_DESCENDING);

if(filectime($saveDirectory.$files[0])>filectime($localDirectory.$locfiles[0])){
	$newest_file = $files[0];
	rename($saveDirectory.$newest_file, $localDirectory.$newest_file);
	$d = compress($localDirectory.$newest_file, $localDirectory.$newest_file, 80,0.5);
}else{
	$newest_file= $locfiles[0];
}

$imagePath=$localDirectory.$newest_file;
echo $newest_file;

function compress($source, $destination, $quality, $percent) { 
	$info = getimagesize($source);
	if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source);
	elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source);
	elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source);
	
	
	$newwidth = $info[0] * $percent;
	$newheight = $info[1] * $percent;

	// Chargement
	$thumb = imagecreatetruecolor($newwidth, $newheight);

	// Redimensionnement
	imagecopyresized($thumb, $image, 0, 0, 0, 0, $newwidth, $newheight, $info[0], $info[1]);

	imagejpeg($thumb, $destination, $quality);
	
	return $destination;
} 



?>