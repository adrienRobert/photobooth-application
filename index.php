<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="style/favicon.ico">

    <title>RFID Photobooth</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="style/dashboard.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body style="width:100%;height:100%;text-align:center;position:absolute;top:0;left:0;padding-top:0px;">
  	<!-- <script src="js/angular.min.js"></script> -->
    <script src="js/jquery.min.js"></script>
    <!-- <script src="../angularBase/ui-bootstrap.min.js"></script> -->
    <script src="js/app.js"></script>

      <div class="form">
        <img src="assets/form.jpg" style="width:1920px;height:auto;position:relative;margin-top:0px;">
        <input type="text" id="firstname" class="formInput form-control" placeholder="Prénom">
        <input type="text" id="lastname" class="formInput form-control" placeholder="Nom" style="top:419px;">
        <input type="text" id="email" class="formInput form-control" placeholder="Courriel" style="top:520px;">
        <input type="text" id="phone" class="formInput form-control" placeholder="Numéro de téléphone" style="top:621px;">
        <button id="readyButton"  style="">Continuer</button>
      </div>

      <div class="countdown" style="display:none;">
        <img src="assets/cdPage2.jpg" style="height:950px;width:auto;display:none;margin-left:300px;" class="countdown">
        <div class="countdown numberCountdown" style="display:none;"></div>
      </div>

    
     <div class="showPic" style="display:none;">
        <img src="assets/shPage.jpg" class="showPic" style="width:1500px;height:auto;margin-top:50px;display:none;">
        <div class="imageContainer showPic" style="display:none;"><img id="image" src="" width="100%" height="auto"></div>
        <button class="customBtn showPic" id="yesButton" style="display:none;top:746px;left:590px;">Accepter la photo</button>
        <button class="customBtn showPic" id="noButton" style="display:none;top:746px;left:940px;">Reprendre la photo</button>

     </div>
    
    <div class="custom" style="display:none;">
      <img class="custom" src="assets/csPage.jpg" style="width:1700px;height:auto;margin-top:50px;display:none;" >

      <table class='frames custom'>
  <?php
    $frameDirectory="frames/";
    $files = scandir($frameDirectory, SCANDIR_SORT_DESCENDING);
    $i=0;
    echo "<tr>";
    foreach ($files as $key => $value) {
      if(strlen($value)>2 && $i <= 3){
        if($i==2){
          echo "</tr><tr>";
        }
        echo "<td class='tdframes'><img class='picFrames' id='' src=''><img class='imgFrames' id='".$i."' src='".$frameDirectory.$value."'></td>";
        $i=$i+1;
      }
    }
    echo "</tr>";
  ?>
    </table>

    <div class="sendMail custom" id="yes"></div>
    <div class="sendMail custom" id="no" style="left:930px;"></div>

   
    </div>

    <div class="thx" style="display:none;">
      <img class="thx" src="assets/thx.jpg" style="width:1920px;height:auto;display:none;" >
    </div>

  </body>
</html>
